//
//  Rational.hpp
//  RationalNumbers
//
//  Created by pp's MacBook  on 16/10/6.
//  Copyright © 2016 pp. All rights reserved.
//

#ifndef Rational_hpp
#define Rational_hpp

#include <stdio.h>
#include <iostream>

using namespace std;

class Rational {

private:
    int m_n; // numerator
    int m_d; // denominator
public:
    Rational();
    Rational(int);
    Rational(int, int);
    
    Rational operator+(const Rational & q);
    Rational operator/(const Rational & q);
    bool operator==(const Rational & q);

    friend ostream& operator<<(ostream& os, const Rational & q);
};

#endif /* Rational_hpp */
