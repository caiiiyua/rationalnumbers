//
//  Rational.cpp
//  RationalNumbers
//
//  Created by pps's MacBook  on 16/10/6.
//  Copyright © 2016 pp. All rights reserved.
//

#include "Rational.hpp"

Rational::Rational() {
    m_n = 0;
    m_d = 1;
}

Rational::Rational(int n) {
    m_n = n;
    m_d = 1;
}

Rational::Rational(int n, int d) {
    m_n = n;
    if (d != 0) {
        m_d = d;
    } else {
        // throw IllegalArgumentException to indicate d should not be 0
        throw "Invalid demoninator";
    }
}

/**
 *  Basically, a/b + c/d = (ad + bc)/bd
 */
Rational Rational::operator+(const Rational &q) {
    Rational tempQ = Rational();

    if (m_d == q.m_d) {
        // Add directly if their denominator is the same
        tempQ.m_d = m_d;
        tempQ.m_n = m_n + q.m_n;
    } else {
        tempQ.m_n = m_n * q.m_d + m_d * q.m_n;
        tempQ.m_d = m_d * q.m_d;
    }
    return tempQ;
}

/**
 *  Basically, a/b / c/d = ad/bc
 */
Rational Rational::operator/(const Rational &q) {
    if (q.m_n == 0) {
        // threw IllegalArgumentException
        throw "Invalid demoninator";
    }
    return Rational(m_n * q.m_d, m_d * q.m_n);
}

/**
 *  Basically, a/b == c/d only if ad == bc
 */
bool Rational::operator==(const Rational &q) {
    return m_n * q.m_d == m_d * q.m_n;
}

ostream & operator<<(ostream &out, const Rational &q) {
    out<<q.m_n<<"/"<<q.m_d;
    return out;
}
