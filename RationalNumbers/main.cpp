//
//  main.cpp
//  RationalNumbers
//
//  Created by pp's MacBook  on 16/10/6.
//  Copyright © 2016 pp. All rights reserved.
//

#include <iostream>
#include "Rational.hpp"

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Hello, World!\n";
    Rational q1 = Rational();
    Rational q2 = Rational(2, 3);
    Rational q3 = Rational(3, -5);
    Rational q4 = Rational(1, 2);
    Rational q5 = Rational(2, 4);
    
    cout<<q1<<"+"<<q2<<" expects 2/3"<<" got "<<q1+q2<<endl;
    cout<<q2<<"/"<<q3<<" expects -10/9"<<" got "<<q2/q3<<endl;
    cout<<q4<<"=="<<q5<<" expects true"<<" got "<<(q4==q5 ? "true" : "false")<<endl;
    cout<<Rational(1, 0);
    return 0;
}
