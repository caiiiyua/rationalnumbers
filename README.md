### RationalNumbers ###

Write and document a class (Rational) in C++ for representing rational numbers. A rational number is any number that can be represented as a fraction e.g. 1/3,   2/4. 

Your class should include operators:

* addition (+) 
* division (/) 
* equality (==)